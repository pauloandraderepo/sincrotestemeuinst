<?php
// define variables and set to empty values
$email = $senha = $mensagemErro = $teste = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $email = test_input($_POST["email"]);
  $senha = $_POST["senha"];
}

if(strlen($email) == 0 || simularLogin($email, $senha)) {
  $mensagemErro = '';
} else {
  $mensagemErro = 'Login inválido, favor verificar';
}

function simularLogin($email, $senha) {
  return ($email === 'teste@meuinstrumento.com' && $senha === 'adm3000');
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>

<!DOCTYPE html>
<html lang="en" style="height: 100%">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>MeuInstrumento.com</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/login.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body style="height: 100%">
  <div class="navbar-fixed" style="z-index:999;">
    <nav class="grey lighten-1" role="navigation" id="main-navigator">
      <div class="nav-wrapper container">
        <a id="logo-container" href="#" class="brand-logo"><img src="logologin.png" class="imagem-logo" alt="logologin"/></a>
        <ul class="right hide-on-med-and-down grey lighten-1" style="height: 100%">
          <li>Ainda não possui cadastro?</li>
          <li><a href="cadastro.html" class="texto-graffite waves-effect teal-text"><u>Cadastre-se agora!</u></a></li>
        </ul>

        <ul id="nav-mobile" class="side-nav grey lighten-1 opacidade90">
          <li><a href="cadastro.html" class="texto-branco waves-effect waves-light"><u>Cadastre-se agora!</u></a></li>
        </ul>
          <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons texto-branco">menu</i></a>
      </div>
    </nav>
  </div>

  <div class="container custom-container">
    <div class="row">
      <div class="col s12 m7">
        <h5>Olá, cê tá bão né</h5>
        <div style="margin-top: 10px">Conheça o nosso app do MeuInstrumento sô!</div>
        <div style="margin-top: 10px">Pega no meu Berimbau!   O download é de grátis!</div>
        <a href="https://play.google.com/store" target="_blank"><img-style="border"><img src="ggp.png" class="imagem-logo"/></img-style="border"></a>
        <a href="http://www.apple.com/br/" target="_blank"><img-style="border"><img src="aps.png" class="imagem-logo"/></img-style="border"></a>
        </div>
          <div class="col s12 m5">
              <form class="col s12" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="row">
                <h5 class="grey-text">Informe aqui seu login</h5>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <input id="email" name="email" type="email" class="validate" required>
                    <label for="email">Email</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <input id="senha" name="senha" type="password" class="validate" required>
                    <label for="senha">Senha</label>
                  </div>
                </div>
                <div class="row right-align">
                  <input type="submit" class="custom-button btn-large grafitte" value="Acessar" />
                </div>
                <input type="hidden" name = "mensagemErro" id="mensagemErro" value="<?php echo $mensagemErro;?>" />
              </form>
        </div>
      </div>
    </div>

  <footer class="page-footer grey lighten-1 rodape">
    <div class="container">
      <div class="row">
        <div class="col s12">
          <a href="https://fb.me/MeuInstrumento" target="_blank"><img-style="border"><img src="face.png" class="imagem-social f-hover opacidade60"/></img-style="border"></a>
          <a href="https://www.instagram.com/meuinstrumento/" target="_blank"><img-style="border"><img src="insta.png" class="imagem-social f-hover opacidade60"/></img-style="border"></a>
          <a href="https://twitter.com/MeuInstrumento_" target="_blank"><img-style="border"><img src="twee.png" class="imagem-social f-hover opacidade60"/></img-style="border"></a>
          <a href="https://www.youtube.com/channel/UC_DKgm6s9x-LQij4gO0yA0Q" target="_blank"><img-style="border"><img src="yout.png" class="imagem-social f-hover opacidade60"/></img-style="border"></a>
          <a href="https://plus.google.com/u/0/116672578113826636765" target="_blank"><img-style="border"><img src="ggl.png" class="imagem-social f-hover opacidade60"/></img-style="border"></a>              
        </div>
      </div>
    </div> 
    <div class="footer-copyright grey darken-2">
      <div class="container">
        <div style="text-align: center;">
         © Todos os direitos reservados ao MeuInstrumento.com
        </div>
      </div>
    </div>
  </footer>

    <!--  Scripts-->
    <script src="js/jquery-2.1.1.min.js"></script>
    <script src="js/materialize.js"></script>
    <script src="js/init.js"></script>
    <script src="js/login.js"></script>
</body>
</html>

