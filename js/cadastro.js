var endpointAutenticacao = 'http:///meuinstr-auth.herokuapp.com/loginUsuario'
// var endpointAutenticacao = 'http://localhost:3000/loginUsuario'
var endpointRecursos = 'https://meuinstrumento-dev.firebaseio.com/'

processarRequisicao(false)

function cadastrar() {
    var nome = $('#nome').val()
    var email = $('#email').val()
    var senha = $('#senha').val()

    if(nome.length > 0 && email.length > 0 && senha.length > 0) {
        mostrarToastMensagem('Por favor aguarde...')
        processarRequisicao(true)
        mockGuardarPreCadastro()

        setTimeout(function() {
            window.location.assign('http://meuinstrumento.com/teste/dadosUsuario.html')
        }, 3000)

    } else {
        mostrarToastMensagem('Por gentileza preencher todos os campos.')
    }

    $('#botaoCadastrar').blur() // materialize change custom color (graffite) on element focus.
}

function validarEmail() {
    var emailInvalido = $('#email').attr('class').indexOf('invalid') !== -1

    if(emailInvalido) {
        $('#email').val('')
    }
}

function mostrarToastMensagem(mensagem) {
	Materialize.toast(mensagem, 3000)
}

function processarRequisicao(carregando) {
    carregando ? $("#iconeCarregando").show() : $("#iconeCarregando").hide()
    carregando ? $("#containerBtnCadastrar").hide() : $("#containerBtnCadastrar").show()
}

function mockGuardarPreCadastro() {
    sessionStorage.setItem('nomeCompleto', $('#nome').val())
    sessionStorage.setItem('email', $('#email').val())
    sessionStorage.setItem('senha', $('#senha').val())
}
