var endpointAutenticacao = 'http:///meuinstr-auth.herokuapp.com/loginUsuario'
// var endpointAutenticacao = 'http://localhost:3000/loginUsuario'
var endpointRecursos = 'https://meuinstrumento-dev.firebaseio.com/'

function showListaCordas() {
	buscarItensCordas()
}

function showListaSopro() {
	buscarItensSopro()
}

function showListaPercussao() {
	buscarItensPercussao()
}

function showListaOutros() {
	buscarItensOutros()
}

function showListaGuitarra() {
	buscarItensGuitarra()
}

function showListaBaixo() {
	buscarItensBaixo()
}

function showListaBateria() {
	buscarItensBateria()
}

function showListaModelos() {
	buscarModelos()
}

function adicionarItem(recurso) {
	var nome = prompt('Digite o nome do novo item')
	if(nome == null || nome === '') {
		return;
	}
	recurso = recurso.toLowerCase()
	var endpoint = constroiUrlRecurso(recurso)
	var request = $.ajax({
		url: endpoint,
		method: 'POST',
		data: JSON.stringify({ id: Date.now(), nome: nome }),
		contentType: "application/json",
		success: function(result) {
			console.log('sucesso no post ' + nome)
			if(recurso === 'sopro') {
				buscarItensSopro()
			} else if (recurso === 'cordas') {
				buscarItensCordas()
			} else if (recurso === 'percussao') {
				buscarItensPercussao()
			} else if (recurso === 'outros') {
				buscarItensOutros()
			} else if (recurso === 'guitarra') {
				buscarItensGuitarra()
			} else if (recurso === 'baixo') {
				buscarItensBaixo()
			} else if (recurso === 'bateria') {
				buscarItensBateria()
			}
		}
	})
}

function adicionarModelo(idMarca, tipoInstrumento) {
	var nome = prompt('Digite o nome de um novo modelo desta marca')
	if(nome == null || nome === '') {
		return;
	}
	var endpoint = constroiUrlRecurso('modelos')
	var request = $.ajax({
		url: endpoint,
		method: 'POST',
		data: JSON.stringify({ id: Date.now(), nome: nome, idMarca: idMarca, tipoInstrumento: tipoInstrumento.toLowerCase() }),
		contentType: "application/json",
	})
}

function deletarItem(id, nome, recurso) {
	var confirmed = confirm('Confirma a exclusão do ' + nome + '?')
	if(confirmed == true) {
		recurso = recurso.toLowerCase()
		var endpoint = constroiUrlRecurso(recurso + '/' + id)
		var request = $.ajax({
			url: endpoint,
			method: 'DELETE',
			contentType: "application/json",
			success: function(result) {
				if(recurso === 'sopro') {
					buscarItensSopro()
				} else if (recurso === 'cordas') {
					buscarItensCordas()
				} else if (recurso === 'percussao') {
					buscarItensPercussao()
				} else if (recurso === 'outros') {
					buscarItensOutros()
				} else if (recurso === 'guitarra') {
					buscarItensGuitarra()
				} else if (recurso === 'baixo') {
					buscarItensBaixo()
				} else if (recurso === 'bateria') {
					buscarItensBateria()
				} else if (recurso === 'modelos') {
					buscarModelos()
				}
			}
		})
	}
}

function alterarItem(id, nome, titulo) {
	$('#tituloModal').empty()
	$('#tituloModal').append(titulo)
	$('#novoNome').val(nome)
	$('#idItem').val(id)
	$('#modalEditarItem').modal('open')
}

function buscarItensCordas() {
	$.get(constroiUrlRecurso('cordas'), function(data){
		$('#container-itens').empty()
		$('#container-itens').append(`<li class="collection-header"><h5>Cordas</h5><div class="right-align">&nbsp;<a href="#!" class="secondary-content" onclick="adicionarItem('Cordas')"><i class="material-icons">add</i></a></div></li>`)
		for(var item in data) {
			if(data[item] != null) {
				$('#container-itens').append(`<li class="collection-item"><div>${data[item].nome}<a href="#!" class="secondary-content" onclick="alterarItem('${item}', '${data[item].nome}', 'Cordas')"><i class="material-icons">mode_edit</i></a><a href="#!" class="secondary-content" onclick="deletarItem('${item}', '${data[item].nome}', 'Cordas')"><i class="material-icons">delete</i></a></div></li>`);			
			}
		}
		$('#container-itens').fadeIn('slow')
	})
}

function buscarItensSopro() {
	$.get(constroiUrlRecurso('sopro'), function(data){
		$('#container-itens').empty()
		$('#container-itens').append(`<li class="collection-header"><h5>Sopro</h5><div class="right-align">&nbsp;<a href="#!" class="secondary-content" onclick="adicionarItem('Sopro')"><i class="material-icons">add</i></a></div></li>`)
		for(var item in data) {
			if(data[item] != null) {
				$('#container-itens').append(`<li class="collection-item"><div>${data[item].nome}<a href="#!" class="secondary-content" onclick="alterarItem('${item}', '${data[item].nome}', 'Sopro')"><i class="material-icons">mode_edit</i></a><a href="#!" class="secondary-content" onclick="deletarItem('${item}', '${data[item].nome}', 'Sopro')"><i class="material-icons">delete</i></a></div></li>`);
			}
		}
		$('#container-itens').fadeIn('slow')	
	})
}

function buscarItensPercussao() {
	$.get(constroiUrlRecurso('percussao'), function(data){
		$('#container-itens').empty()
		$('#container-itens').append(`<li class="collection-header"><h5>Percussão</h5><div class="right-align">&nbsp;<a href="#!" class="secondary-content" onclick="adicionarItem('Percussao')"><i class="material-icons">add</i></a></div></li>`)
		for(var item in data) {
			if(data[item] != null) {
				$('#container-itens').append(`<li class="collection-item"><div>${data[item].nome}<a href="#!" class="secondary-content" onclick="alterarItem('${item}', '${data[item].nome}', 'Percussao')"><i class="material-icons">mode_edit</i></a><a href="#!" class="secondary-content" onclick="deletarItem('${item}', '${data[item].nome}', 'Percussao')"><i class="material-icons">delete</i></a></div></li></div></li>`);
			}
		}
		$('#container-itens').fadeIn('slow')	
	})
}

function buscarItensOutros() {
	$.get(constroiUrlRecurso('outros'), function(data){
		$('#container-itens').empty()
		$('#container-itens').append(`<li class="collection-header"><h5>Outros</h5><div class="right-align">&nbsp;<a href="#!" class="secondary-content" onclick="adicionarItem('Outros')"><i class="material-icons">add</i></a></div></li>`)
		for(var item in data) {
			if(data[item] != null) {
				$('#container-itens').append(`<li class="collection-item"><div>${data[item].nome}<a href="#!" class="secondary-content" onclick="alterarItem('${item}', '${data[item].nome}', 'Outros')"><i class="material-icons">mode_edit</i></a><a href="#!" class="secondary-content" onclick="deletarItem('${item}', '${data[item].nome}', 'Outros')"><i class="material-icons">delete</i></a></div></li>`);
			}
		}
		$('#container-itens').fadeIn('slow')
	})
}

function buscarItensGuitarra() {
	$.get(constroiUrlRecurso('guitarra'), function(data){
		$('#container-itens').empty()
		$('#container-itens').append(`<li class="collection-header"><h5>Guitarra</h5><div class="right-align">&nbsp;<a href="#!" class="secondary-content" onclick="adicionarItem('Guitarra')"><i class="material-icons">add</i></a></div></li>`)
		for(var item in data) {
			if(data[item] != null) {
				$('#container-itens').append(`<li class="collection-item"><div>${data[item].nome}<a href="#!" class="secondary-content" onclick="adicionarModelo('${item}', 'Guitarra')"><i class="material-icons">settings_input_component</i></a><a href="#!" class="secondary-content" onclick="alterarItem('${item}', '${data[item].nome}', 'Guitarra')"><i class="material-icons">mode_edit</i></a><a href="#!" class="secondary-content" onclick="deletarItem('${item}', '${data[item].nome}', 'Guitarra')"><i class="material-icons">delete</i></a></div></li>`);
			}
		}
		$('#container-itens').fadeIn('slow')	
	})
}

function buscarItensBaixo() {
	$.get(constroiUrlRecurso('baixo'), function(data){
		$('#container-itens').empty()
		$('#container-itens').append(`<li class="collection-header"><h5>Baixo</h5><div class="right-align">&nbsp;<a href="#!" class="secondary-content" onclick="adicionarItem('Baixo')"><i class="material-icons">add</i></a></div></li>`)
		for(var item in data) {
			if(data[item] != null) {
				$('#container-itens').append(`<li class="collection-item"><div>${data[item].nome}<a href="#!" class="secondary-content" onclick="adicionarModelo('${item}', 'Baixo')"><i class="material-icons">settings_input_component</i></a><a href="#!" class="secondary-content" onclick="alterarItem('${item}', '${data[item].nome}', 'Baixo')"><i class="material-icons">mode_edit</i></a><a href="#!" class="secondary-content" onclick="deletarItem('${item}', '${data[item].nome}', 'Baixo')"><i class="material-icons">delete</i></a></div></li>`);
			}
		}
		$('#container-itens').fadeIn('slow')
	})
}

function buscarItensBateria() {
	$.get(constroiUrlRecurso('bateria'), function(data){
		$('#container-itens').empty()
		$('#container-itens').append(`<li class="collection-header"><h5>Bateria</h5><div class="right-align">&nbsp;<a href="#!" class="secondary-content" onclick="adicionarItem('Bateria')"><i class="material-icons">add</i></a></div></li>`)
		for(var item in data) {
			if(data[item] != null) {
				$('#container-itens').append(`<li class="collection-item"><div>${data[item].nome}<a href="#!" class="secondary-content" onclick="adicionarModelo('${item}', 'Bateria')"><i class="material-icons">settings_input_component</i><a href="#!" class="secondary-content" onclick="alterarItem('${item}', '${data[item].nome}', 'Bateria')"><i class="material-icons">mode_edit</i></a><a href="#!" class="secondary-content" onclick="deletarItem('${item}', '${data[item].nome}', 'Bateria')"><i class="material-icons">delete</i></a></div></li>`);
			}
		}
		$('#container-itens').fadeIn('slow')	
	})
}

function buscarModelos() {
	$.get(constroiUrlRecurso('modelos'), function(data){
		$('#container-itens').empty()
		$('#container-itens').append(`<li class="collection-header"><h5>Modelos</h5></li>`)
		for(var item in data) {
			if(data[item] != null) {
				renderizarModelo(item, data[item])
			}
		}
		$('#container-itens').fadeIn('slow')
	})
}

function concluirAlteracao() {
	var recurso = $('#tituloModal').text().toLowerCase()
	recurso = (recurso === 'percussão' ? 'percussao' : recurso)
	var id = $('#idItem').val()
	var novoNome = $('#novoNome').val()
	var endpoint = constroiUrlRecurso(recurso + '/' + id + '/')
	var request = $.ajax({
		url: endpoint,
		method: 'PATCH',
		data: JSON.stringify({ id: id, nome: novoNome }),
		contentType: "application/json",
		success: function(result) {
			if(recurso === 'sopro') {
				buscarItensSopro()
			} else if (recurso === 'cordas') {
				buscarItensCordas()
			} else if (recurso === 'percussao') {
				buscarItensPercussao()
			} else if (recurso === 'outros') {
				buscarItensOutros()
			} else if (recurso === 'guitarra') {
				buscarItensGuitarra()
			} else if (recurso === 'baixo') {
				buscarItensBaixo()
			} else if (recurso === 'bateria') {
				buscarItensBateria()
			} else if (recurso === 'modelos') {
				buscarModelos()
			}
		}
	})
}

function renderizarModelo(index, data) {
	$.get(constroiUrlRecurso(data.tipoInstrumento+'/'+data.idMarca), function(dadosMarca){
		$('#container-itens').append(`<li class="collection-item"><div>${data.tipoInstrumento.toUpperCase()} - ${dadosMarca.nome} - ${data.nome}<a href="#!" class="secondary-content" onclick="deletarItem('${index}', '${data.nome}', 'Modelos')"><i class="material-icons">delete</i></a></div></li>`);
	})
}

function mostrarToastMensagem(mensagem) {
	Materialize.toast(mensagem, 3000)
}

function autenticarUsuario() {
	var email = $('#email').val()
	var password = $('#password').val()

	if(email != null && email.length > 0 && password != null && password.length > 0) {

		$.ajax({
			url: endpointAutenticacao,
			method: 'POST',
			data: JSON.stringify({ email, password }),
			contentType: "application/json",
			success: function(response) {
				if(response.error != null) {
					mostrarToastMensagem(response.error.message)
				} else {
					sessionStorage.setItem('token', response.token)
					sessionStorage.setItem('validade', response.validade)
					$('#modalAutenticacao').modal('close')
				}
			},
			error: function(error) {
				mostrarToastMensagem(error.responseText)
			}
		})

	} else {
		mostrarToastMensagem('Favor informar os dados de acesso!')
	}
}

function constroiUrlRecurso(recurso) {
	if(sessionStorage != null && sessionStorage.getItem('token') != null && sessionStorage.getItem('token').length > 0) {
		return endpointRecursos + recurso + '.json?auth=' + sessionStorage.getItem('token') 
	} else {
		mostrarToastMensagem('Nenhuma sessão ativa, por favor realize um novo login')
	}
}

$('#modalEditarItem').modal()
$('#modalAutenticacao').modal({
	dismissible: false
})

if(sessionStorage == null || sessionStorage.getItem('token') == null || sessionStorage.getItem('token').length == 0) {
	$('#modalAutenticacao').modal('open')
}
