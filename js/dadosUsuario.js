var endpointAutenticacao = 'http:///meuinstr-auth.herokuapp.com/loginUsuario'
// var endpointAutenticacao = 'http://localhost:3000/loginUsuario'
var endpointRecursos = 'https://meuinstrumento-dev.firebaseio.com/'



$('#nome').val(sessionStorage.getItem('nomeCompleto'))
$('#email').val(sessionStorage.getItem('email'))
$('#senha').val(sessionStorage.getItem('senha'))

function validarEmail() {
    var emailInvalido = $('#email').attr('class').indexOf('invalid') !== -1

    if(emailInvalido) {
        $('#email').val('')
    }
}

function tratarMascaraCampo() {
  var tipoMascara = $('#tipoIdentificacao').val()
  $('#identidade').val('')

  if(tipoMascara == 'CPF') {
    $('#identidade').mask('999.999.999-99')
  } else if(tipoMascara == 'RG') {
    $('#identidade').mask('9.999.999')
  } else {
    $('#identidade').mask('999999999999999')
  }


}
