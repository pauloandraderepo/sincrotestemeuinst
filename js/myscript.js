var fadeGraffite = false;

function tratarVisibilidadeScrollTop() {
	if($(window).scrollTop() == 0) {
   		$('#scrollToTop').addClass('scale-out');
   } else {
   		$('#scrollToTop').removeClass('scale-out');
   }
};

function configurarEfeitoGradienteCabecalho() {
	$(window).scroll(function() {
	   if($(window).scrollTop() > 80) {
	       $('#main-navigator').animate({opacity: 0.8}, 10);
	   } else if($(window).scrollTop() <= 80) {
	   		$('#main-navigator').animate({opacity: 1}, 10);
	   }
	   tratarVisibilidadeScrollTop();
	});
};

function configurarBotaoScrollTop() {
	$('#scrollToTop').click(function() {
	    var body = $("html, body");
	    body.stop().animate({scrollTop:0}, '500', 'swing');
	    event.preventDefault();
	});
	tratarVisibilidadeScrollTop();
};

function abrirTermosPrivacidade() {
	$('#modalPrivacidade').modal('open');
	event.preventDefault();
};
 
configurarEfeitoGradienteCabecalho();
configurarBotaoScrollTop();
$('#modalPrivacidade').modal();
$('#modalTermosUso').modal();